<?php

namespace App\Controller;

use App\Entity\Film;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;


/**
 * @Route("/film", name="film")
 */
class FilmController extends Controller
{
    /**
     * @Route("/", name="list_film")
     */
    public function index()
    {
        $films = $this->getDoctrine()
            ->getRepository(film::class)
            ->findAll();
        $data = $this->serializer->serialize($films, 'json');

        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        return new Response($data);
    }

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

}
